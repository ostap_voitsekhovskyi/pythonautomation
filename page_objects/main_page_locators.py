from selenium.webdriver.common.by import By

"""Eskin Alex"""


class MainPageLocators(object):
    """A class for main page locators. All main page locators should come here"""

    # Button Sign In_Main_Page:
    sign_in_button = (By.XPATH, "//*[.='Sign In']")
    page_loader = (By.CSS_SELECTOR, ".header__logo")

    # Buttons SignUp_Main_Page:
    sign_up_header_button = (By.LINK_TEXT, "Start For Free")
    sign_up_primary_button = (By.LINK_TEXT, "Start Learning")
    sign_up_primary_button_Second = (By.LINK_TEXT, "Start Teaching")

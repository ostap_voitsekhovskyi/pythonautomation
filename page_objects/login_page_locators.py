from selenium.webdriver.common.by import By

"""Eskin Alex"""


class LoginPageLocators(object):
    """A class for main page locators. All main page locators should come here"""

    # Log In Page locators:
    login_field_locator = (By.CSS_SELECTOR, "[formcontrolname='email']")
    password_field_locator = (By.CSS_SELECTOR, "[formcontrolname='password']")
    login_button_locator = (By.CSS_SELECTOR, ".login__form-btn")

    page_loader = (By.CSS_SELECTOR, ".login__content-title")

from selenium.webdriver.remote.webdriver import WebDriver
from selenium.webdriver.support.wait import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC

from page_objects.abstract_web_page import AbstractWebPage
from page_objects.login_page import LoginPage
from page_objects.main_page_locators import MainPageLocators
from web_elements.buttons import InputButton

"""Eskin Alex"""


class MainPage(AbstractWebPage):

    def __init__(self, web_driver: WebDriver):
        super().__init__(web_driver)

    @property
    def sign_in_button(self):
        sign_in_button_web_element = self.web_driver.find_element(*MainPageLocators.sign_in_button)
        return InputButton(sign_in_button_web_element)

    def open_login_page(self):
        self.focus_web_element_js(self.sign_in_button)
        self.sign_in_button.click()
        return LoginPage(self.web_driver)

    def wait_until_page_loaded(self):
        WebDriverWait(self.web_driver, 30).until(
            EC.visibility_of_element_located(MainPageLocators.page_loader))

from selenium.webdriver.remote.webdriver import WebDriver
from selenium.webdriver.support.wait import WebDriverWait
from selenium.webdriver.support import expected_conditions as ec
from page_objects.abstract_web_page import AbstractWebPage
from page_objects.home_page import HomePage
from page_objects.login_page_locators import LoginPageLocators
from web_elements.buttons import InputButton
from web_elements.input_field import InputField

"""Eskin Alex"""


class LoginPage(AbstractWebPage):
    def __init__(self, web_driver: WebDriver):
        super().__init__(web_driver)

    @property
    def email_input(self):
        return InputField(self.web_driver.find_element(*LoginPageLocators.login_field_locator))

    @property
    def password_input(self):
        return InputField(self.web_driver.find_element(*LoginPageLocators.password_field_locator))

    @property
    def sign_in_button(self):
        return InputButton(self.web_driver.find_element(*LoginPageLocators.login_button_locator))

    # Login Action:
    def login_to_app(self, user, password):
        self.email_input.set_value(user)
        self.password_input.set_value(password)
        self.sign_in_button.click()
        return HomePage(self.web_driver)

    def wait_until_page_loaded(self):
        WebDriverWait(self.web_driver, 10).until(
            ec.visibility_of_element_located(LoginPageLocators.page_loader))

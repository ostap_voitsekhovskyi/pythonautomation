from selenium.common.exceptions import NoSuchElementException, TimeoutException
from selenium.webdriver import ActionChains
from selenium.webdriver.remote.webdriver import WebDriver
from selenium.webdriver.support.wait import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC

from web_elements.abstract_web_element import AbstractWebElement


class AbstractWebPage(object):

    def __init__(self, web_driver: WebDriver):
        self.web_driver = web_driver
        self.wait_until_page_loaded()

    def wait_until_page_loaded(self):
        pass

    def focus_web_element(self, web_element: AbstractWebElement):
        ActionChains(self.web_driver).move_to_element(web_element.web_element).click(
            web_element.web_element).perform()

    def focus_web_element_js(self, web_element: AbstractWebElement):
        self.web_driver.execute_script("arguments[0].focus();", web_element.web_element)

    def fill_field_js(self, web_element: AbstractWebElement, text: str):
        self.web_driver.execute_script("arguments[0].value = '{}';".format(text), web_element.web_element)

    def focus_and_fill_field(self, web_element: AbstractWebElement, text: str):
        ActionChains(self.web_driver) \
            .move_to_element(web_element.web_element) \
            .click() \
            .send_keys_to_element(web_element.web_element, text) \
            .perform()

    def focus_and_click(self, web_element: AbstractWebElement):
        ActionChains(self.web_driver) \
            .move_to_element(web_element.web_element) \
            .click(web_element.web_element) \
            .perform()

    # @staticmethod
    # def is_element_shown(web_element: WebElement):
    #     try:
    #         return web_element.is_displayed()
    #     except NoSuchElementException:
    #         return False

    @property
    def is_element_visible(self, locator: tuple):
        try:
            return WebDriverWait(self.web_driver, 10).until(
            EC.visibility_of_element_located(locator)).is_displayed()
        except NoSuchElementException:
            return False
        except TimeoutException:
            return False
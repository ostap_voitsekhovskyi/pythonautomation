from selenium.webdriver.common.by import By

"""Eskin Alex"""


class HomePageLocators(object):
    """A class for main page locators. All main page locators should come here"""

    page_loader = (By.CSS_SELECTOR, ".library__text")

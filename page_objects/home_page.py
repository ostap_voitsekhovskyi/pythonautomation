from selenium.webdriver.remote.webdriver import WebDriver
from selenium.webdriver.support.wait import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC


from page_objects.abstract_web_page import AbstractWebPage
from page_objects.home_page_locators import HomePageLocators
from selenium.common.exceptions import NoSuchElementException
from time import sleep
from selenium.webdriver.common.by import By

"""Eskin Alex"""


class HomePage(AbstractWebPage):
    def __init__(self, web_driver: WebDriver):
        super().__init__(web_driver)

    @property
    def is_loaded(self):
        try:
            return self.web_driver.find_element(*HomePageLocators.page_loader).is_displayed()
        except NoSuchElementException:
            return False

    def open_dropdown(self):
        #locator = (By.XPATH, "//*[@class = 'home__header-nav-link non--avatar ng-star-inserted']")
        #self.web_driver.find_element(*locator).click()
        element = self.web_driver.find_element_by_xpath("//*[@class = 'home__header-nav-link non--avatar ng-star-inserted']");
        element.click()
        sleep(2)
        self.web_driver.find_element()

    def wait_until_page_loaded(self):
        assert self.is_element_visible(HomePageLocators.page_loader)

from selenium.webdriver.remote.webelement import WebElement
from web_elements.abstract_web_element import AbstractWebElement

"""Eskin Alex"""


class InputField(AbstractWebElement):

    def __init__(self, web_element: WebElement):
        super().__init__(web_element)

    def set_value(self, text: str):
        self.web_element.send_keys(text)

    def click(self):
        self.web_element.click()

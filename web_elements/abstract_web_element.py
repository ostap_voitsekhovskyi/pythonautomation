from selenium.common.exceptions import NoSuchElementException
from selenium.webdriver.remote.webelement import WebElement


class AbstractWebElement(object):

    # Initialize web_element
    def __init__(self, web_element: WebElement):
        self.__element = web_element

    # Return web_element as object
    @property
    def web_element(self):
        return self.__element

    # Return tag name of element
    @property
    def tag_name(self):
        return self.web_element.tag_name

    # Return text from element
    @property
    def text(self):
        return self.web_element.text

    # Return value from element
    @property
    def value(self):
        return self.web_element.get_attribute("value")

    # Check if element is displayed, return boolean
    def is_displayed(self):
        return self.__element.is_displayed()

    # Check if element is enabled, return boolean
    def is_enabled(self):
        return self.__element.is_enabled()

    # Check id element is selected, return boolean
    def is_selected(self):
        return self.__element.is_selected()

    # Get specific attribute of element, return string
    def get_attribute(self, attr):
        return self.__element.get_attribute(attr)

    @property
    def is_shown(self):
        try:
            return self.web_element.is_displayed()
        except NoSuchElementException:
            return False

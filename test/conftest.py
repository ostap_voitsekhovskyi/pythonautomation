import sys

import pytest
import os
from selenium import webdriver
from selenium.webdriver.firefox.options import Options as fOptions
from page_objects.main_page import MainPage


def pytest_addoption(parser):
    parser.addoption("--firefox",
                     action='store_true',
                     default=False,
                     help="Start Firefox WebDriver")
    parser.addoption("--ie",
                     action='store_true',
                     default=False,
                     help="Start Internet Explorer WebDriver")
    parser.addoption("--chrome",
                     action='store_true',
                     default=False,
                     help="Start Google Chrome WebDriver")


@pytest.fixture(scope='class', autouse=True)
def web_driver_setup(request):
    drivers_dir = os.path.abspath(os.path.join(os.path.abspath(os.path.curdir), "web_drivers"))

    if pytest.config.getoption("--firefox"):
        request.cls.webdriver = webdriver.Firefox
        web_driver_file = "geckodriver"
        if 'win' in sys.platform:
            web_driver_file += '.exe'
        request.cls.webdriver_path = os.path.join(drivers_dir, web_driver_file)

    elif pytest.config.getoption("--chrome"):
        request.cls.webdriver = webdriver.Chrome
        web_driver_file = "chromedriver"
        if 'win' in sys.platform:
            web_driver_file += '.exe'
        request.cls.webdriver_path = os.path.join(drivers_dir, web_driver_file)

    elif pytest.config.getoption("--ie"):
        request.cls.webdriver = webdriver.Ie
        request.cls.webdriver_path = os.path.join(drivers_dir, "IEDriverServer.exe")

    else:
        raise ValueError("Incorrect browser")


@pytest.fixture(scope='function', autouse=True)
def load_app(request):
    if isinstance(request.cls.webdriver, webdriver.Chrome):
        chrome_options = webdriver.ChromeOptions()
        chrome_options.add_argument("--disable-infobars")
        chrome_options.add_argument('disable-infobars')
        request.cls.initialized_webdriver = request.cls.webdriver(executable_path=request.cls.webdriver_path,
                                                                  chrome_options=chrome_options)
    elif isinstance(request.cls.webdriver, webdriver.Firefox):
        _browser_profile = webdriver.FirefoxProfile()
        _browser_profile.set_preference("dom.webnotifications.enabled", False)
        options = fOptions()
        request.cls.initialized_webdriver = request.cls.webdriver(executable_path=request.cls.webdriver_path,
                                                                  firefox_options=options,
                                                                  firefox_profile=_browser_profile)
    else:
        request.cls.initialized_webdriver = request.cls.webdriver(executable_path=request.cls.webdriver_path)

    request.cls.initialized_webdriver.maximize_window()
    request.cls.initialized_webdriver.get("https://diprella.com/")
    request.cls.main_app = MainPage(request.cls.initialized_webdriver)

    def driver_close():
        request.cls.initialized_webdriver.quit()

    request.addfinalizer(driver_close)

from time import sleep


# virtualenv venv
# venv\scripts\activate
# python setup.py install
# python setup.py install_webdrivers
# pytest --log-level=DEBUG test/test_debug.py --firefox


class TestDebug(object):

    def test_debug(self):
        login_page = self.main_app.open_login_page()
        home_page = login_page.login_to_app("ola.sheremeta@gmail.com", "asdzxc123")
        home_page.open_dropdown()

        # assert home_page.is_loaded, "Home page isn't loaded"

    def test_login(self):
        login_page = self.main_app.open_login_page()
        home_page = login_page.login_to_app("ola.sheremeta@gmail.com", "asdzxc123")
        assert home_page.is_loaded, "Home page isn't loaded"

    def test_login_negative(self):
        home_page = self.main_app.open_login_page().login_to_app("ola.sheremeta@gmail.com", "asdzxc123")
        assert home_page.is_loaded, "Home page isn't loaded"

        sleep(5)
